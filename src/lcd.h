#ifndef SO_LCD_H
#define SO_LCD_H

#include "defines.h"
#include <Arduino.h>
#include <LiquidCrystal.h>

#include "controller.h"

class Lcd
{
public:
  Lcd(byte rs, byte en, byte d4, byte d5, byte d6, byte d7);
  void init(Controller * ctr);
  void draw();
  void setCursorPosition(uint8_t pos);
  void toggleBlink();
  void nextSec();
private:
  void drawMark(uint8_t pos);
  void drawIcon(uint8_t iconID);
  void drawHead();
  void drawMainMenu();
  // void drawHeatMenu();
  void drawProgMenu();
  void drawCodeMenu();
  void drawConfMenu();
  void drawMemMenu();
  void drawTimerPoint(uint8_t num,bool withCursor = false);
  void drawMemPoint(uint8_t num,bool withCursor = false);
  void drawTime(uint16_t mins,bool dots = true);
private:
  byte _pinRs;
  byte _pinEn;
  byte _pinD4;
  byte _pinD5;
  byte _pinD6;
  byte _pinD7;

  Controller * _ctr;
  LiquidCrystal * _lcd;
  bool blinkOn;
  byte sec6;
};

#endif
