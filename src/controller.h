#ifndef SO_CONTROLLER_H
#define SO_CONTROLLER_H

#include <Arduino.h>
#include "defines.h"

class Lcd;
class MAX6675;

class Controller
{
public:
  struct TimerPoint {
    uint16_t temp;
    uint16_t time;
    uint8_t grow;
  };
  struct RunState {
    uint8_t timer;
    unsigned long startMillis;
    unsigned long lastMillis;
    unsigned long lastTemp1mMillis;
    uint16_t startTemp;
    uint16_t lastTemp;
    bool growLastDone;
    unsigned long lastOn;
    unsigned long lastOff;
    bool growDone;
    unsigned long growDoneAt;
  };
  struct Config {
    uint8_t minHeaterOn;
    uint8_t minHeaterOff;
    uint8_t heaterMode;
    uint8_t okDelta;
  };
  struct Programm {
    TimerPoint timers[PROG_TIMERS];
  };

  Controller();
  void init(Lcd * lcd, MAX6675 * sens);
  Config config();
  void configure();
  void prog2mem(uint8_t memPos);
  void mem2prog(uint8_t memPos);
  void mem2zero(uint8_t memPos);
  void update();
  void gotoMenu(uint8_t menuID);
  void setWM(uint8_t workModeID);

  bool buttonHold();
  uint8_t button();
  uint8_t menu();
  uint8_t cursor();
  uint8_t mode();
  uint8_t code();
  TimerPoint tp(uint8_t point);
  uint8_t tpSize();
  uint8_t tpCursor();

  //is growing
  bool rsGrow();
  bool rsGrowUp();
  uint8_t rsGrowSize();
  uint16_t rsCurGoToTemp();
  uint16_t rsTemp();
  uint16_t rsTime();
  uint16_t rsDone();
  uint16_t rsMinsTotal();
  uint16_t rsMinsLast();
  uint16_t rsMinsGrow();

  Programm mem(uint8_t i);

  uint16_t sensor();
  bool heater();

  void clearProgram();
  void runProgramm();

  void setHeater(bool state);
private:
  void updateRun();
  void updateHeaters();
  void runTimerPoint(byte i);

  void updateSensors();
  void updateButton();
  void updateCursor();
  void updateMenu();
private:
  Config _conf;

  Lcd * _lcd;
  MAX6675 * _sens;
  unsigned long lastButtonMillis;
  unsigned long lastSensorsMillis;
  unsigned long lastButtonDown;
  uint8_t curButton;
  uint8_t curMenu;
  uint8_t curCursor;
  uint8_t curTpCursor;
  uint8_t lastButton;
  uint8_t workMode;

  uint16_t sensorVal;

  bool heaterState;

  TimerPoint tpRun;
  TimerPoint programm[PROG_TIMERS];
  uint8_t codeCursor;
  RunState runState;
  Programm memProgs[PROG_MEM_TIMERS];
};

#endif
