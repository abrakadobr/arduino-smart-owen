#include "lcd.h"

byte i_logo[8] = { 0x00, 0x04, 0x0A, 0x15, 0x11, 0x00, 0x0E, 0x1F };
byte i_cursor[8] = { 0x00, 0x1C, 0x1E, 0x1F, 0x1E, 0x1C, 0x00, 0x00 };
byte i_temp0[8] = { 0x04, 0x0A, 0x0A, 0x0A, 0x0A, 0x11, 0x11, 0x0E };
byte i_conf[8] = { 0x00, 0x0F, 0x1B, 0x17, 0x1F, 0x1F, 0x1F, 0x00 };
byte i_prog0[8] = { 0x00, 0x0E, 0x13, 0x15, 0x11, 0x0E, 0x00, 0x00 };
byte i_run[8] = { 0x00, 0x14, 0x16, 0x17, 0x16, 0x14, 0x00, 0x00 };
//byte i_load[8] = { 0x00, 0x0E, 0x1F, 0x17, 0x13, 0x0E, 0x00, 0x1F };// { 0x07, 0x03, 0x01, 0x0E, 0x13, 0x15, 0x11, 0x0E };
//byte i_save[8] = { 0x00, 0x0E, 0x19, 0x1D, 0x1F, 0x0E, 0x00, 0x1F };//{ 0x04, 0x06, 0x07, 0x0E, 0x13, 0x15, 0x11, 0x0E };
byte i_mem[8] = { 0x00, 0x15, 0x1F, 0x1F, 0x1F, 0x15, 0x00, 0x00};
byte i_updown[8] = { 0x04, 0x0E, 0x15, 0x04, 0x04, 0x15, 0x0E, 0x04};
// byte i_down[8] = { 0x04, 0x04, 0x04, 0x04, 0x04, 0x15, 0x0E, 0x04};

Lcd::Lcd(byte rs, byte en, byte d4, byte d5, byte d6, byte d7)
{
  sec6 = 0;
  _pinRs = rs;
  _pinEn = en;
  _pinD4 = d4;
  _pinD5 = d5;
  _pinD6 = d6;
  _pinD7 = d7;
  this->_lcd = new LiquidCrystal(_pinRs,_pinEn,_pinD4,_pinD5,_pinD6,_pinD7);
}

void Lcd::init(Controller * ctr)
{
  this->_ctr = ctr;
  this->_lcd->begin(16, 2);
  this->_lcd->createChar(ICON_LOGO, i_logo);
  this->_lcd->createChar(ICON_CURSOR, i_cursor);
  this->_lcd->createChar(ICON_TEMP0, i_temp0);
  this->_lcd->createChar(ICON_PROG0, i_prog0);
  this->_lcd->createChar(ICON_CONF, i_conf);
  this->_lcd->createChar(ICON_RUN, i_run);
  this->_lcd->createChar(ICON_MEM, i_mem);
  this->_lcd->createChar(ICON_UPDOWN, i_updown);
  // this->_lcd->createChar(ICON_DOWN, i_down);

  this->_lcd->setCursor(0, 0);
  this->drawIcon(ICON_LOGO);
  this->_lcd->print(" Alfa Owen");
  this->_lcd->setCursor(0, 1);
  this->_lcd->print("TAKATAN S&R");
}

void Lcd::drawHead()
{
  //first line
  this->_lcd->clear();
  this->_lcd->setCursor(0, 0);
  this->drawIcon(ICON_LOGO);
  if (this->_ctr->mode()==WM_NONE)
    this->drawIcon(ICON_SQ0);
  if (this->_ctr->mode()==WM_RUN)
  {
    if (_ctr->rsGrow())
      this->drawIcon(ICON_UPDOWN);
    else
      this->drawIcon(ICON_PROG0);
  }
  /*
  if (this->_ctr->heater())
  {
    if (this->blinkOn)
      this->drawIcon(ICON_TEMP0);
    else
      this->_lcd->print(" ");
  } else {
    this->_lcd->print(" ");
  }
  */

}

void Lcd::drawMainMenu()
{
  this->drawHead();
  if (this->_ctr->mode()==WM_NONE)
    this->_lcd->print(" wait ");
  bool line1CornerBusy = false;
  if (this->_ctr->mode()==WM_RUN)
  {
    // programm state current/total timers
    if (sec6 == 0||sec6==1)
    {
      uint8_t lastTimer = this->_ctr->tpSize();
      uint8_t tmNum = this->_ctr->code() +1;
      uint8_t ltNum = lastTimer+1;
      this->_lcd->print(" run ");
      this->_lcd->print(tmNum);
      this->_lcd->print("/");
      this->_lcd->print(ltNum);
    }
    // state info
    if (sec6==2||sec6==3)
    {
      // grow state gotoTemp/growSize growTime
      if (_ctr->rsGrow())
      {
        _lcd->print(" ");
        // drawIcon(ICON_UPDOWN);
        // _lcd->print(" G");
        _lcd->print(_ctr->rsCurGoToTemp());
        drawIcon(ICON_DEG);
        _lcd->print("/");
        _lcd->print(_ctr->rsGrowSize());
        drawIcon(ICON_DEG);

        //time of groing
        _lcd->setCursor(11,0);
        drawTime(_ctr->rsMinsGrow(), sec6==2);
        line1CornerBusy = true;
      } else {
        // timer info
        if (_ctr->rsTime()>0)
        {
          // timer is NOT infinitive
          if (sec6==2)
            _lcd->print(" ");
          else
            drawIcon(ICON_PROG0);
          drawTime(_ctr->rsMinsLast(), sec6==2);
        } else {
          // timer is infinitive
          if (sec6==2)
            _lcd->print(" ");
          else
            drawIcon(ICON_TEMP0);
          drawTime(_ctr->rsDone(), sec6==2);
        }
      }
    }

    //total working timer
    if (sec6==4||sec6==5)
    {
      _lcd->print(" ");
      drawTime(_ctr->rsMinsTotal(), sec6==4);
    }

    //destination temperature at end of line
    if (!line1CornerBusy)
    {
      uint8_t dtl = 2;
      if (this->_ctr->rsTemp()>9)
        dtl = 3;
      if (this->_ctr->rsTemp()>99)
        dtl = 4;
      _lcd->setCursor(16-dtl,0);
      _lcd->print(this->_ctr->rsTemp());
      this->drawIcon(ICON_DEG);
    }

  }

  //second line
  this->_lcd->setCursor(0,1);
  drawMark(0);
  this->drawIcon(ICON_PROG0);
  if (this->_ctr->cursor()==0)
    this->_lcd->print("timers ");
  else
    this->_lcd->print(" ");
  drawMark(1);
  this->drawIcon(ICON_CONF);
  if (this->_ctr->cursor()==1)
    this->_lcd->print("config");
  else
    this->_lcd->print(" ");

  //current sensor temperature on second line
  uint8_t stl = 2;
  if (this->_ctr->sensor()>9)
    stl = 3;
  if (this->_ctr->sensor()>99)
    stl = 4;
  this->_lcd->setCursor(16-stl,1);
  this->_lcd->print(this->_ctr->sensor());
  if (_ctr->heater()&&blinkOn)
    drawIcon(ICON_TEMP0);
  else
    this->drawIcon(ICON_DEG);
}

void Lcd::drawProgMenu()
{
  //first line
  this->drawHead();
  this->_lcd->print("timers");
  // lcd.print(millis()/1000);
  this->_lcd->setCursor(16-PROG_TIMERS, 0);
  for(uint8_t i=0;i<PROG_TIMERS;i++)
    this->drawTimerPoint(i);


  //second line
  this->_lcd->setCursor(0,1);

  if (this->_ctr->cursor()==0)
  {
    drawMark(0);
    this->drawIcon(ICON_ARROW_LEFT);
    this->_lcd->print("back ");
  } else {
    this->drawIcon(ICON_ARROW_LEFT);
  }
  if (this->_ctr->cursor()==1)
  {
    this->_lcd->print(" ");
    drawMark(1);
    this->drawIcon(ICON_PROG0);
    this->_lcd->print("set ");
  } else {
    this->drawIcon(ICON_PROG0);
  }
  if (this->_ctr->cursor()==2)
  {
    this->_lcd->print(" ");
    drawMark(2);
    this->drawIcon(ICON_RUN);
    this->_lcd->print("run ");
  } else {
    this->drawIcon(ICON_RUN);
  }
  if (this->_ctr->cursor()==3)
  {
    this->_lcd->print(" ");
    drawMark(3);
    this->drawIcon(ICON_SQ0);
    this->_lcd->print("clear ");
  } else {
    this->drawIcon(ICON_SQ0);
  }
  if (this->_ctr->cursor()==4)
  {
    this->_lcd->print(" ");
    drawMark(4);
    this->drawIcon(ICON_MEM);
    this->_lcd->print("memory ");
  } else {
    this->drawIcon(ICON_MEM);
  }
  /*
  if (this->_ctr->cursor()==5)
  {
    this->_lcd->print(" ");
    drawMark(5);
    this->drawIcon(ICON_PROG_LOAD);
    this->_lcd->print("load");
  } else {
    this->drawIcon(ICON_PROG_LOAD);
  }
  */

}

void Lcd::drawMemMenu()
{
  //first line
  this->drawHead();

  if (_ctr->cursor()==0)
  {
    _lcd->print("memory");
  } else {
    Controller::Programm p = _ctr->mem(_ctr->cursor()-1);
    if (p.timers[0].temp==0)
    {
      _lcd->print("[");
      _lcd->print(_ctr->cursor());
      _lcd->print("] empty");
    } else {
      byte timers = 0;
      for(uint8_t i=0;i<PROG_MEM_TIMERS;i++)
      {
        if (p.timers[i].temp==0)
          break;
        timers++;
      }
      _lcd->print("[");
      _lcd->print(_ctr->cursor());
      _lcd->print("] ");
      _lcd->print(timers);
      if (timers>1)
        _lcd->print(" timers");
      else
        _lcd->print(" timer");
    }
  }

  // this->_lcd->print("memory");

  //second line
  this->_lcd->setCursor(0,1);

  if (this->_ctr->cursor()==0)
  {
    drawMark(0);
    this->drawIcon(ICON_ARROW_LEFT);
    this->_lcd->print("back ");
  } else {
    this->drawIcon(ICON_ARROW_LEFT);
  }
  _lcd->setCursor(16-PROG_MEM_TIMERS,1);
  for(uint8_t i=0;i<PROG_MEM_TIMERS;i++)
    this->drawMemPoint(i);

}


void Lcd::drawCodeMenu()
{
  //first line
  this->drawHead();
  this->_lcd->print("timer ");
  this->_lcd->print(this->_ctr->tpCursor());

  // lcd.print(millis()/1000);
  this->_lcd->setCursor(15-PROG_TIMERS, 0);
  drawMark(0);
  for(uint8_t i=0;i<PROG_TIMERS;i++)
    this->drawTimerPoint(i,true);


  //second line
  this->_lcd->setCursor(0,1);
  Controller::TimerPoint tp = this->_ctr->tp(this->_ctr->tpCursor());

  if (this->_ctr->cursor()==1)
  {
    drawMark(1);
    this->drawIcon(ICON_ARROW_LEFT);
    this->_lcd->print("back ");
  } else {
    this->drawIcon(ICON_ARROW_LEFT);
  }
  if (this->_ctr->cursor()==2)
  {
    this->_lcd->print(" ");
    drawMark(2);
    this->drawIcon(ICON_PROG0);
    this->_lcd->print(tp.time);
    this->_lcd->print("min ");
  } else {
    this->drawIcon(ICON_PROG0);
    this->_lcd->print(tp.time);
    // this->_lcd->print("min ");
  }
  if (this->_ctr->cursor()==3)
  {
    this->_lcd->print(" ");
    drawMark(3);
    this->drawIcon(ICON_TEMP0);
    this->_lcd->print(tp.temp);
    this->drawIcon(ICON_DEG);
    this->_lcd->print(" ");
  } else {
    this->drawIcon(ICON_TEMP0);
    this->_lcd->print(tp.temp);
  }
  if (this->_ctr->cursor()==4)
  {
    this->_lcd->print(" ");
    drawMark(4);
    this->drawIcon(ICON_ARROW_RIGHT);
    this->_lcd->print(tp.grow);
    this->drawIcon(ICON_DEG);
    this->_lcd->print("/m ");
  } else {
    this->drawIcon(ICON_ARROW_RIGHT);
    this->_lcd->print(tp.grow);
  }
}

void Lcd::drawConfMenu()
{
  this->drawHead();
  // if (this->_ctr->mode()==WM_NONE)
    this->_lcd->print(" config ");
  /*
  if (this->_ctr->mode()==WM_RUN)
  {
    uint8_t lastTimer = this->_ctr->tpSize();
    uint8_t tmNum = this->_ctr->code() +1;
    uint8_t ltNum = lastTimer+1;
    this->_lcd->print(" run ");
    this->_lcd->print(tmNum);
    this->_lcd->print("/");
    this->_lcd->print(ltNum);
  }
  */

  //second line
  _lcd->setCursor(0,1);
  Controller::Config cfg = _ctr->config();
  if (this->_ctr->cursor()==0)
  {
    drawMark(0);
    this->drawIcon(ICON_ARROW_LEFT);
    this->_lcd->print("back ");
  } else {
    this->drawIcon(ICON_ARROW_LEFT);
  }
  if (this->_ctr->cursor()==1)
  {
    this->_lcd->print(" ");
    drawMark(1);
    this->drawIcon(ICON_PROG0);
    this->_lcd->print("on");
    this->_lcd->print(cfg.minHeaterOn);
    this->_lcd->print("sec ");
  } else {
    this->drawIcon(ICON_PROG0);
  }
  if (this->_ctr->cursor()==2)
  {
    this->_lcd->print(" ");
    drawMark(2);
    // this->drawIcon(ICON_PROG0);
    this->_lcd->print("Xoff");
    this->_lcd->print(cfg.minHeaterOff);
    this->_lcd->print("sec ");
  } else {
    // this->drawIcon(ICON_PROG0);
    this->_lcd->print("X");
  }
  /*
  if (this->_ctr->cursor()==3)
  {
    this->_lcd->print(" ");
    drawMark(3);
    this->drawIcon(ICON_TEMP0);
    if (cfg.heaterMode==HM_BOT)
      this->_lcd->print("BOT ");
    if (cfg.heaterMode==HM_TOP)
      this->_lcd->print("TOP ");
    if (cfg.heaterMode==HM_BOTH)
      this->_lcd->print("BOTH ");
  } else {
    this->drawIcon(ICON_TEMP0);
  }
  */
  if (this->_ctr->cursor()==3)
  {
    this->_lcd->print(" ");
    drawMark(3);
    // this->drawIcon(ICON_PROG0);
    this->_lcd->print("Delta");
    this->_lcd->print(cfg.okDelta);
    this->drawIcon(ICON_DEG);
  } else {
    this->_lcd->print("D");
  }



}

void Lcd::drawMark(uint8_t pos)
{
  if (pos==this->_ctr->cursor()&&this->blinkOn)
    this->drawIcon(ICON_CURSOR);
  else
    this->_lcd->print(" ");
}

void Lcd::drawIcon(uint8_t iconID)
{
  this->_lcd->write((byte)iconID);
}

void Lcd::toggleBlink()
{
  this->blinkOn = !this->blinkOn;
}

void Lcd::drawTimerPoint(uint8_t num, bool withCursor)
{
  Controller::TimerPoint tp = this->_ctr->tp(num);
  if (withCursor&&this->blinkOn&&num==this->_ctr->tpCursor())
  {
    this->drawIcon(ICON_BLANK);
  } else {
    if (tp.temp==0)
    {
      this->_lcd->print("x");
    } else {
      this->drawIcon(ICON_PROG0);
    }
  }
}


void Lcd::drawMemPoint(uint8_t num, bool withCursor)
{
  Controller::Programm p = _ctr->mem(num);
  if (blinkOn&&num==_ctr->cursor()-1)
  {
    _lcd->print(_ctr->cursor());
    // this->drawIcon(ICON_BLANK);
  } else {
    if (p.timers[0].temp==0)
    {
      this->_lcd->print("x");
    } else {
      this->drawIcon(ICON_MEM);
    }
  }
}

void Lcd::draw()
{
  if (this->_ctr->menu()==MENU_MAIN)
    return this->drawMainMenu();
  if (this->_ctr->menu()==MENU_PROG)
    return this->drawProgMenu();
  if (this->_ctr->menu()==MENU_CODE)
    return this->drawCodeMenu();
  if (this->_ctr->menu()==MENU_CONF)
    return this->drawConfMenu();
  if (this->_ctr->menu()==MENU_MEM)
    return this->drawMemMenu();
}

void Lcd::nextSec()
{
  sec6++;
  if (sec6==6)
    sec6=0;
}

void Lcd::drawTime(uint16_t mins,bool dots)
{
  uint8_t hour = mins/60;
  uint8_t min = mins - hour*60;
  if (hour<10)
    _lcd->print("0");
  _lcd->print(hour);
  if (dots)
    _lcd->print(":");
  else
    _lcd->print(" ");
  if (min<10)
    _lcd->print("0");
  _lcd->print(min);
}
