#include "controller.h"
#include "lcd.h"
#include <max6675.h>
#include <EEPROM.h>

#define PIN_HEATER_OFF HIGH
#define PIN_HEATER_ON LOW

uint8_t cursorMenuMax[] = {1,0,3,4,4,3,PROG_MEM_TIMERS};

Controller::Controller()
{
  clearProgram();
  EEPROM.get(0,_conf);
  EEPROM.get(sizeof(Controller::Config),memProgs);
  if (_conf.heaterMode == 0||_conf.heaterMode>3)
  {
    // _conf.heaterMode = HM_BOT;
    _conf.minHeaterOn = 10;
    _conf.minHeaterOff = 10;
    configure();
  }
}

Controller::Config Controller::config(){return _conf;}
void Controller::configure()
{
  EEPROM.put(0, _conf);
}
Controller::Programm Controller::mem(uint8_t i)
{
  Controller::Programm ret;
  memset(&ret, 0, sizeof(Controller::Programm));
  if (i>=PROG_MEM_TIMERS)
    return ret;
  return memProgs[i];
}

void Controller::mem2prog(uint8_t memPos)
{
  for(byte i=0;i<PROG_TIMERS;i++)
    programm[i] = memProgs[memPos].timers[i];
}
void Controller::prog2mem(uint8_t memPos)
{
  for(byte i=0;i<PROG_TIMERS;i++)
    memProgs[memPos].timers[i] = programm[i];
  EEPROM.put(sizeof(_conf),memProgs);
}
void Controller::mem2zero(uint8_t memPos)
{
  memset(&memProgs[memPos],0,sizeof(Controller::Programm));
  EEPROM.put(sizeof(_conf),memProgs);
}

uint8_t Controller::button(){return curButton;}
uint8_t Controller::menu(){return curMenu;}
uint8_t Controller::cursor(){return curCursor;}
uint8_t Controller::code(){return codeCursor;}
uint8_t Controller::tpCursor(){return curTpCursor;}
uint8_t Controller::mode(){return workMode;}
uint16_t Controller::sensor() {return sensorVal;}
bool Controller::heater(){return heaterState;}
Controller::TimerPoint Controller::tp(uint8_t point){return programm[point];}
uint8_t Controller::tpSize() {
  uint8_t lastTimer = 0;
  for(uint8_t i=0;i<PROG_TIMERS;i++)
  {
    if (programm[i].temp > 0)
      lastTimer = i;
  }
  return lastTimer;
}

void Controller::init(Lcd * lcd, MAX6675 * sens)
{
  _lcd = lcd;
  _sens = sens;
  lastButtonMillis = 0;
  lastSensorsMillis = 0;
  curButton = BTN_NONE;
  lastButton = BTN_NONE;
  curMenu = MENU_MAIN;
  workMode = WM_NONE;
  pinMode(PIN_HEATER, OUTPUT);
  digitalWrite(PIN_HEATER, PIN_HEATER_OFF);
  setHeater(false);
#ifdef DOSERIAL
  Serial.println("TAKATAN HEATER");
  Serial.print("EEPROM:");
  Serial.print(EEPROM.length());
  Serial.print("; Config size: ");
  Serial.print(sizeof(Controller::Config));
  Serial.print("; Programm size: ");
  Serial.print(sizeof(Controller::Programm));
#endif
}

void Controller::gotoMenu(uint8_t menuID)
{
  curMenu = menuID;
  curCursor = 0;
  //code menu default "back" position is 1
  if (menuID == MENU_CODE)
    curCursor = 1;
  curTpCursor = 0;
}

void Controller::setWM(uint8_t workModeID)
{
  workMode = workModeID;
}

void Controller::update()
{
  //read sensors
  updateSensors();
  //read buttons
  updateButton();
  updateCursor();
  updateMenu();
  updateRun();
  updateHeaters();
}

void Controller::updateSensors()
{
  //do update by interval only
  if (millis()-lastSensorsMillis<SENSOR_UPDATE_INTERVAL)
    return;
  lastSensorsMillis = millis();
  sensorVal = _sens->readCelsius()*1;
}

void Controller::updateButton()
{
  curButton = BTN_NONE;
  //programmable delay for 100ms
  //from last click to prevent doubles/missclicks/etc
  if (millis()-lastButtonMillis<250)
    return;
  lastButtonMillis = millis();

  int analog0 = analogRead(0);
  //select button
  // if (analog0>1000)
    // curButton = BTN_NONE;
  if (analog0>600&&analog0<1000)
    curButton = BTN_SELECT;
  if (analog0>400&&analog0<600)
    curButton = BTN_LEFT;
  if (analog0>240&&analog0<400)
    curButton = BTN_DOWN;
  if (analog0>90&&analog0<240)
    curButton = BTN_UP;
  if (analog0<90)
    curButton = BTN_RIGHT;

  if (curButton!=BTN_NONE&&lastButton==BTN_NONE)
    lastButtonDown = millis();

  lastButton = curButton;
}

bool Controller::buttonHold()
{
  return lastButton!=BTN_NONE && millis() - lastButtonDown > 3000;
}

void Controller::updateCursor()
{
  if (button()==BTN_LEFT)
  {
    curCursor--;
    if (curCursor > 10)
      curCursor = cursorMenuMax[curMenu];
  }
  if (button()==BTN_RIGHT)
  {
    curCursor++;
    if (curCursor>cursorMenuMax[curMenu])
      curCursor = 0;
  }
}

void Controller::updateMenu()
{
  if (menu()==MENU_MAIN)
  {
    //for main menu only BTN_SELECT matter
    if (button()!=BTN_SELECT)
      return;
    // if (cursor()==0)
      // return gotoMenu(MENU_HEAT);
    if (cursor()==0)
      return gotoMenu(MENU_PROG);
    if (cursor()==1)
      return gotoMenu(MENU_CONF);
  }
  //menu timers
  if (menu()==MENU_PROG)
  {
    if (cursor()==0&&button()==BTN_SELECT)
      return gotoMenu(MENU_MAIN);
    if (cursor()==1&&button()==BTN_SELECT)
      return gotoMenu(MENU_CODE);
    if (cursor()==2&&button()==BTN_SELECT)
      return runProgramm();
    if (cursor()==3&&button()==BTN_SELECT)
      return clearProgram();
    if (cursor()==4&&button()==BTN_SELECT)
      return gotoMenu(MENU_MEM);
  }
  //menu memory
  if (menu()==MENU_MEM)
  {
    if (cursor()==0&&button()==BTN_SELECT)
      return gotoMenu(MENU_PROG);
    if (cursor()>0&&button()==BTN_UP)
      return mem2zero(cursor()-1);
    if (cursor()>0&&button()==BTN_DOWN)
      return prog2mem(cursor()-1);
    if (cursor()>0&&button()==BTN_SELECT)
    {
      mem2prog(cursor()-1);
      gotoMenu(MENU_PROG);
      curCursor = 2;
      return;
    }

  }


  //menu code timer
  if (menu()==MENU_CODE)
  {
    if (cursor()==1&&button()==BTN_SELECT)
      return gotoMenu(MENU_PROG);
    if (cursor()==0&&button()==BTN_UP)
    {
      curTpCursor++;
      if (curTpCursor >= PROG_TIMERS)
        curTpCursor = 0;
      return;
    }
    if (cursor()==0&&button()==BTN_DOWN)
    {
      curTpCursor--;
      if (curTpCursor >= PROG_TIMERS)
        curTpCursor = PROG_TIMERS;
      return;
    }
    if (cursor()==2)
    {
      if (button()==BTN_UP)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].time + inc;
        if (nx > 999)
          nx = 999;
        programm[curTpCursor].time = nx;
        return;
      }
      if (button()==BTN_DOWN)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].time-inc;
        if (nx < 0||nx > 3000)
          nx = 0;
        programm[curTpCursor].time = nx;
        return;
      }
    }
    if (cursor()==3)
    {
      if (button()==BTN_UP)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].temp + inc;
        if (nx > 999)
          nx = 999;
        programm[curTpCursor].temp = nx;
        return;
      }
      if (button()==BTN_DOWN)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].temp-inc;
        if (nx < 0||nx > 3000)
          nx = 0;
        programm[curTpCursor].temp = nx;
        return;
      }
    }

    if (cursor()==4)
    {
      if (button()==BTN_UP)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].grow + inc;
        if (nx > 999)
          nx = 999;
        programm[curTpCursor].grow = nx;
        return;
      }
      if (button()==BTN_DOWN)
      {
        int inc = 1;
        if (buttonHold())
          inc = 20;
        uint16_t nx = programm[curTpCursor].grow-inc;
        if (nx < 0||nx > 3000)
          nx = 0;
        programm[curTpCursor].grow = nx;
        return;
      }
    }
  }

  //menu conf
  if (menu()==MENU_CONF)
  {
    if (cursor()==0&&button()==BTN_SELECT)
      return gotoMenu(MENU_MAIN);
    if (cursor()==1&&button()==BTN_UP)
    {
      _conf.minHeaterOn++;
      if (_conf.minHeaterOn >= 100)
        _conf.minHeaterOn = 0;
      configure();
      return;
    }
    if (cursor()==0&&button()==BTN_DOWN)
    {
      _conf.minHeaterOn--;
      if (_conf.minHeaterOn >= 100)
        _conf.minHeaterOn = 99;
      configure();
      return;
    }
    if (cursor()==2)
    {
      if (button()==BTN_UP)
      {
        _conf.minHeaterOff++;
        if (_conf.minHeaterOff >= 100)
          _conf.minHeaterOff = 0;
        configure();
        return;
      }
      if (button()==BTN_DOWN)
      {
        _conf.minHeaterOff--;
        if (_conf.minHeaterOff >= 100)
          _conf.minHeaterOff = 99;
        configure();
        return;
      }
    }
    if (cursor()==3)
    {
      if (button()==BTN_UP)
      {
        _conf.okDelta++;
        if (_conf.okDelta >= 30)
          _conf.okDelta = 1;
        configure();
        return;
      }
      if (button()==BTN_DOWN)
      {
        _conf.okDelta--;
        if (_conf.okDelta == 0)
          _conf.okDelta = 30;
        configure();
        return;
      }
    }

  }

}

void Controller::setHeater(bool state)
{
  heaterState = state;
}

void Controller::clearProgram()
{
  for(uint8_t i=0;i<PROG_TIMERS;i++)
  {
    programm[i].temp = 0;
    programm[i].time = 0;
    programm[i].grow = 0;
  }
  codeCursor = 0;
  runState.timer = 0;
  runState.startMillis = 0;
  runState.lastMillis = 0;
  runState.startTemp = 0;
  runState.lastOn = 0;
  runState.lastOff = 0;
  setWM(WM_NONE);
}

void Controller::runTimerPoint(byte i)
{
  if (i>PROG_TIMERS)
  {
    //WTF? breaking
    setWM(WM_NONE);
    return;
  }
  TimerPoint * p = &programm[i];
  //timer is workable?
  if (p->temp==0)
  {
    setWM(WM_NONE);
    return;
  }
  unsigned long mls = millis();
  codeCursor = i;
  runState.timer = i;
  runState.growDoneAt = 0;
  runState.startMillis = mls;
  runState.lastMillis = mls;
  runState.startTemp = sensor();
  runState.lastTemp = runState.startTemp;
  runState.growDone = false;
  runState.growLastDone = false;
  runState.lastOn = 0;
  runState.lastOff = 0;
  setWM(WM_RUN);
}

void Controller::runProgramm()
{
  runTimerPoint(0);
  gotoMenu(MENU_MAIN);
}

void Controller::updateRun()
{
  if (workMode == WM_NONE)
  {
    setHeater(false);
    return;
  }
  unsigned long mls = millis();
  bool heating = heater();
  //should do minimum on
  if (heating&&mls-runState.lastOn<_conf.minHeaterOn*1000)
    return;
  //should do minimum off
  if (!heating&&mls-runState.lastOff<_conf.minHeaterOff*1000)
    return;

  uint16_t temp = sensor();

  TimerPoint * p = &programm[runState.timer];

  //by default - go to timer temperature directly
  uint16_t gotoTemp = p->temp;
  //we need change temp carefully
  if (p->grow&&!runState.growDone)
  {
    gotoTemp = runState.lastTemp;
    //if 1 minute update done
    if (mls-runState.lastTemp1mMillis>=60000&&runState.growLastDone) //1min
    {
      gotoTemp = runState.startTemp<p->temp?gotoTemp+p->grow:gotoTemp-p->grow;
      runState.lastTemp1mMillis = mls;
      runState.lastTemp = gotoTemp;
      runState.growLastDone = false;
    }
  }

  if (p->temp>=runState.startTemp)
  {
    //GO UP
    //still not reach temp, continue
    if (temp<gotoTemp+_conf.okDelta)
    {
      //TEMP LOWER THEN REQUIRED
      //all goes fine
      if (heating)
        return;
      //temp not enough and we not heating?
      //need switch heaters on
      setHeater(true);
      //and remember lastOn
      runState.lastOn = mls;
    } else {
      //TEMP HIGHER THEN REQUIRED
      //for sure, need to switch heaters off
      setHeater(false);
      //store lastOff time
      runState.lastOff = mls;
      //and store that we reached temperature, if growing
      runState.growLastDone = true;
      //now let's check our states
      //we finished grow?
      if (!runState.growDone)
      {
        //grow was NOT marked as finished
        //did we really rich point temp?
        if (gotoTemp >= p->temp)
        {
          runState.growDone = true;
          runState.growDoneAt = mls;
        }
      }
      //timer not infinitive and finished?
      if (runState.growDone&&p->time!=0&&mls-runState.growDoneAt>=p->time*60000)
        runTimerPoint(runState.timer+1); //run next or end
    }
  } else {
    //GO DOWN
    //still not reach temp, continue
    if (temp>gotoTemp-_conf.okDelta)
    {
      //TEMP HIGHER THEN REQUIRED
      //all goes fine
      if (!heating)
        return;
      //temp too much and we heating?
      //need switch heaters off
      setHeater(false);
      //and remember lastOn
      runState.lastOff = mls;
    } else {
      //TEMP LOWER THEN REQUIRED
      //for sure, need to switch heaters on
      setHeater(true);
      //store lastOff time
      runState.lastOn = mls;
      //and store that we reached temperature, if growing
      runState.growLastDone = true;
      //now let's check our states
      //we finished grow-down?
      if (!runState.growDone)
      {
        //grow was NOT marked as finished
        //did we really rich point temp?
        if (gotoTemp <= p->temp)
        {
          runState.growDone = true;
          runState.growDoneAt = mls;
        }
      }
      //timer not infinitive and finished?
      if (runState.growDone&&p->time!=0&&mls-runState.growDoneAt>=p->time*60000)
        runTimerPoint(runState.timer+1); //run next or end
    }

  }


}

void Controller::updateHeaters()
{
  if (heater())
    digitalWrite(PIN_HEATER, PIN_HEATER_ON);
  else
    digitalWrite(PIN_HEATER, PIN_HEATER_OFF);
}

bool Controller::rsGrow() { return !runState.growDone; }
bool Controller::rsGrowUp() { return runState.startTemp < programm[runState.timer].temp;}
uint8_t Controller::rsGrowSize() { return programm[runState.timer].grow; }
uint16_t Controller::rsCurGoToTemp() { return runState.lastTemp; };
uint16_t Controller::rsTemp() { return programm[runState.timer].temp; };
uint16_t Controller::rsTime() { return programm[runState.timer].time; };
uint16_t Controller::rsDone() {
  if (runState.growDoneAt==0)
    return 0;
   return (millis()-runState.growDoneAt)/60000;
 };

uint16_t Controller::rsMinsTotal()
{
  return (millis() - runState.startMillis)/60000;
}

uint16_t Controller::rsMinsGrow()
{
  unsigned long diff = runState.startMillis;
  if (runState.growDone)
  {
    diff = runState.growDoneAt - diff;
  } else {
    diff = millis() - diff;
  }
  return diff/60000;
}

uint16_t Controller::rsMinsLast()
{
  if (!runState.growDone)
    return programm[runState.timer].time;
  return programm[runState.timer].time - (millis() - runState.growDoneAt)/60000;
}
