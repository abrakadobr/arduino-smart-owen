#include "defines.h"
#include "lcd.h"
#include "controller.h"
#include <max6675.h>

MAX6675 sens(PIN_SENSOR_SCK,PIN_SENSOR_CS,PIN_SENSOR_SO);
Lcd lcd(PIN_LCD_RS,PIN_LCD_EN,PIN_LCD_D4,PIN_LCD_D5,PIN_LCD_D6,PIN_LCD_D7);
Controller controller;

unsigned long lastMillis = 0;
byte blink500 = 0;
byte sec = 0;

bool xx = false;

void setup() {
#ifdef DOSERIAL
  Serial.begin(9600);
#endif
  lcd.init(&controller);
  controller.init(&lcd,&sens);
  controller.setWM(WM_NONE);
  delay(3000);
  controller.gotoMenu(MENU_MAIN);
}



void loop() {
  controller.update();
  if(millis()-lastMillis>500)
  {
    lastMillis = millis();
    lcd.toggleBlink();
    sec++;
    if (sec==2)
    {
      sec = 0;
      lcd.nextSec();
    }
    lcd.draw();
  }
}
