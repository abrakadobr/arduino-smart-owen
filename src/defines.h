#ifndef SO_DEFINES_H
#define SO_DEFINES_H

// serial debug
// #define DOSERIAL
//#define DODEBUG //requires DOSERIAL enabled

// memory config
// number of timers in programm
#define PROG_TIMERS 7
// number of programm saved in memory
#define PROG_MEM_TIMERS 9

// pins config
// lcd
#define PIN_LCD_RS 8
#define PIN_LCD_EN 9
#define PIN_LCD_D4 4
#define PIN_LCD_D5 5
#define PIN_LCD_D6 6
#define PIN_LCD_D7 7
// sensor
#define PIN_SENSOR_SCK 12
#define PIN_SENSOR_SO 10
#define PIN_SENSOR_CS 11
// heater
#define PIN_HEATER 3

// working config
#define SENSOR_UPDATE_INTERVAL 1000

// other defines
// menus
#define MENU_MAIN 0
#define MENU_LOADING 1
#define MENU_HEAT 2
#define MENU_PROG 3
#define MENU_CODE 4
#define MENU_CONF 5
#define MENU_MEM 6
// buttons
#define BTN_NONE 0
#define BTN_LEFT 1
#define BTN_RIGHT 2
#define BTN_UP 3
#define BTN_DOWN 4
#define BTN_SELECT 5
#define BTN_CHECK 9
// working modes
#define WM_NONE 0
#define WM_RUN 1
// icons
// specials
#define ICON_LOGO 0
#define ICON_CURSOR 1
#define ICON_TEMP0 2
#define ICON_PROG0 3
#define ICON_CONF 4
#define ICON_RUN 5
#define ICON_MEM 6
#define ICON_UPDOWN 7
// #define ICON_DOWN 8

// from existing lcd chars memory
#define ICON_BLANK 255
#define ICON_ARROW_RIGHT 126
#define ICON_ARROW_LEFT 127
#define ICON_SQ0 161
#define ICON_SQ1 164
#define ICON_DEG 223

#endif
